plinth (0.3.1.git.20131208-1) UNRELEASED; urgency=low

  * Updated to new git version from Nick Daly based on commit
    68e186deff9eeb2a8ccffdb0e4b4704b229f7478 .
    - Remove patch correct-issue-tracker.diff now included upstream.
    - Updated patches actions-path.diff and program-paths.diff to match
      changes done upstream.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 17 Nov 2013 13:07:21 +0100

plinth (0.3.0.0.git.20131117-1) unstable; urgency=low

  * Updated to new git version from Nick Daly based on commit
    7f3b1a62c81f760da465497030b68d77139406d7.
    - Add new dependencies libjs-jquery and libjs-modernizr to plinth.
      Patch from James Valleroy.
    - Add new dependencies on python-passlib (>= 1.6.1) and python-bcrypt.
  * Remove now obsolete disable-override-config patch
  * Updated program-paths.diff patch to match new upstream source.
  * Add new patch actions-path.diff to use correct path to actions scripts.
  * Add new patch correct-issue-tracker.diff to use correct URL to current
    upstream github repository.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 17 Nov 2013 13:07:21 +0100

plinth (0.3.0.0.git.20131101-2) unstable; urgency=low

  * Rewrite config to get plinth starting out of the box.  New patches
    program-paths and disable-override-config.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 02 Nov 2013 07:54:37 +0100

plinth (0.3.0.0.git.20131101-1) unstable; urgency=low

  * Updated to new git version from Nick Daly based on commit
    b9b4e0a2ec21edc1b1f73cffc905463a96c18f25.
  * Drop patch install-actions-lib made obsolete by latest upstream
    changes.
  * Depend on pandoc-data | pandoc (<= 1.11.1-3) to make sure
    documentation can be built with the latest pandoc package in
    unstable.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 01 Nov 2013 13:14:41 +0100

plinth (0.3.0.0.git.20131028-1) unstable; urgency=low

  * Updated to new git version from Nick Daly based on commit
    0296a1a99cb1ad0a21729ea37fd53e171ee60614.
    - Drops local copies of javascript libraries also available from
      Debian packages.
  * Add new dependency python-contract needed by new upstream version.
  * Reduce the versioned python-withsqlite dependency from
    0.0.0~git.20130929-1 to 0.0.0~git.20130929, to also accept the
    0.0.0~git.20130929-1~pere.0 version currently available from the
    non-debian repo.
  * New patch install-actions-lib to fix install target (Upstream
    issue #41).

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 30 Oct 2013 22:25:25 +0100

plinth (0.3.0.0.git.20131010-1) unstable; urgency=low

  * Updated to new git version from Nick Daly based on
    commit 5ec749af8e5cb2480556e6926e239972ac890b4c
  * Dropped patch debpathes now merged upstream.
  * Changed depend on python-withsqlite to (>= 0.0.0~git.20130929-1),
    making sure a version with support for more than one table in
    one sqlite file is available.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 10 Oct 2013 22:51:34 +0200

plinth (0.0.0~git.20130928-1) unstable; urgency=low

  * Updated to new git version from Nick Daly.
  * Drop patches keep-vendor-dir.diff, handle-unknown-users.diff,
    sudo-not-exmachina.diff and app-owncloud.diff now merged upstream.
  * Drop workaround for keep-vendor-dir.diff from rules file.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 28 Sep 2013 22:55:36 +0200

plinth (0.0.0~git.20130925-2) unstable; urgency=low

  * Depend on python-withsqlite (>= 0.0.0~git.20130915-2) to make sure a
    version with support for the check_same_thread constructor option is
    available.
  * New patch handle-unknown-users.diff to make sure unknown users
    are handled exactly like incorrect passwords when login fail.
  * New patch app-owncloud.diff to add owncloud support to Plinth.
  * Adjusted rules to make sure actions/* scripts are executable.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 27 Sep 2013 09:06:38 +0200

plinth (0.0.0~git.20130925-1) unstable; urgency=low

  [ Tzafrir Cohen ]
  * Initial release. (Closes: #722093)

  [ Petter Reinholdtsen ]
  * New patch keep-vendor-dir.diff to avoid removing directories that
    should survive the clean Makefile target.
  * Add workaround in rules addressing the problem caused by
    keep-vendor-dir.diff being applied after 'make clean' is executed.
  * New patch sudo-not-exmachina.diff to drop the exmachina dependency,
    and adjust binary dependencies and the debpathes patch to cope with
    this.  Drop dependency on augeas-tools, no longer used with this
    patch.
  * Set priority to optional, as the package do not conflict with anything.

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 26 Sep 2013 09:14:54 +0200
